"""
Tests for upt.restraint.wrapper.watcher module.

Test restraint execution through xml updates.

We did not have tests for those updates.

We want to simulate a restraint execution changes the
status of the job.xml file.

The file contains the following elements:

* 1 Recipe with 3 tests
* 1 Recipe with 2 tests


The first recipe will finish correctly, and the second one will
fail due to a localwatchdog in the first task, the second one will
pass.

Table summary with results
------------------------------------------------------------------
| Type   | ID |   status  | result |
| Recipe | 1  | Completed |  PASS  |
| Task   | 1  | Completed |  None  |
| Task   | 2  | Completed |  PASS  |
| Task   | 3  | Completed |  PASS  |
| Recipe | 2  |  Aborted  |  FAIL  |
| Task   | 1  |  Aborted  |  FAIL  |
| Task   | 2  | Completed |  PASS  |
"""
from importlib.resources import files
from pathlib import Path
import shutil
import tempfile
from threading import Semaphore
import time
import unittest
from unittest import mock

from watchdog.observers import Observer

from upt.const import RSTRNT_JOB_XML
from upt.logger import colorize
from upt.restraint.wrapper.watcher import Handler

ASSETS = files(__package__) / 'assets/restraint_execution_states'
FILES = [
    'state_1.xml',
    'state_2.xml',
    'state_3.xml',
    'state_4.xml',
    'state_5.xml',
    'state_6.xml',
]


class TestHandler(unittest.TestCase):
    """Test Handler."""

    def setUp(self):
        """Setup."""
        self.temp_dir = tempfile.mkdtemp()
        self.job_file = Path(self.temp_dir, RSTRNT_JOB_XML)

        # Create a generator to iterate into steps
        self.states = self.update_state()
        # Set the initial status
        shutil.copy((ASSETS / 'state_0.xml'), self.job_file)

        # Prepare the observer
        self.observer = Observer()
        self.event_handler = Handler(Semaphore(1))
        self.event_handler.init_job_watcher(self.job_file)

        # Run observer
        self.observer.schedule(self.event_handler, self.temp_dir, recursive=False)
        self.observer.start()

    def tearDown(self):
        """Clean up."""
        self.observer.stop()
        shutil.rmtree(self.temp_dir)

    def update_state(self):
        """Update state."""
        for file in FILES:
            shutil.copy((ASSETS / file), self.job_file)
            # Without the delay, the test fails
            # We need to wait a tittle until the handler does the job
            time.sleep(0.2)
            yield

    @mock.patch('upt.restraint.wrapper.actions.ActionOnResult.format_msg')
    def test_print_task_result_output(self, mock_format_msg):
        """Test restraint task result print out."""
        test_cases = (
            ('state_1', []),
            ('state_2', [
                [1, 1, colorize('SKIP'.ljust(20)), '.', 'task_1.1_1']
            ]),
            ('state_3', [
                [1, 2, colorize('PASS'.ljust(20)), '.', 'task_1.2_1'],
                [1, 2, colorize('PASS'.ljust(20)), '.', 'task_1.2_2'],
            ]),
            ('state_4', [
                [1, 3, colorize('PASS'.ljust(20)), '.', 'task_1.3_1']
            ]),
            ('state_5', [
                [2, 1, colorize('PASS'.ljust(20)), '.', 'task_2.1_1'],
                [2, 1, colorize('FAIL'.ljust(20)), '.', 'task_2.1_2'],
                [2, 1, colorize('WARN'.ljust(20)), '.', '/10_localwatchdog']
            ]),
            ('state_6', [
                [2, 2, colorize('PASS'.ljust(20)), '.', 'task_2.2_1'],
                [2, 2, colorize('PASS'.ljust(20)), '.', 'task_2.2_1'],
            ]),
        )
        for (description, args_list), _ in zip(test_cases, self.states):
            with self.subTest(description):
                if not args_list:
                    mock_format_msg.not_called()
                else:
                    for args in args_list:
                        mock_format_msg.assert_any_call(*args)

    @mock.patch('builtins.print')
    def test_print_task_lwd_output(self, mock_print):
        """Test restraint task lwd print out."""
        test_cases = (
            ('state_1', []),
            ('state_2', []),
            ('state_3', []),
            ('state_3', []),
            ('state_4', []),
            ('state_5', [['#2: task_id 1 hit LWD.']]),
            ('state_6', []),
        )
        for (description, args_list), _ in zip(test_cases, self.states):
            with self.subTest(description):
                if args_list:
                    for args in args_list:
                        mock_print.assert_any_call(*args)
