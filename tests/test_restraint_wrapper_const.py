"""Test cases for upt.restraint.wrapper.const module."""
import unittest

from upt.restraint.wrapper import const


class TestRstConst(unittest.TestCase):
    """Ensure regexes work."""

    def test_watchdog_regex(self):
        """Ensure regex for watchdog message matches sample data."""
        test_str = "Recipe 8232299 exceeded lab watchdog timer [restraint-error-quark, 4]"
        result = const.RGX_LAB_WATCHDOG.match(test_str)

        self.assertIsNotNone(result)

    def test_others_regexes(self):
        """Ensure regexes can find messages in sample stdout data."""
        data = """Could not resolve hostname
        Error writing to ssh channel
        Unable to find matching host for recipe
        connect to host1 failure'
        """
        for regex in const.ERROR_INDICES:
            result = regex.findall(data)
            self.assertIsNotNone(result)
